#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define SERVER_PORT 31472
#define MAX_LINE 256

int main(int argc, char *argv[]) {
  struct hostent *hp;
  struct sockaddr_in sin;
  char *host;
  char send_buf[MAX_LINE], recv_buf[MAX_LINE];
  int s;
  int send_len, recv_len;
  if (argc == 2) {
    host = argv[1];
  } else {
    fprintf(stderr, "usage: ./client host\n");
    exit(1);
  }

  /* Translate host name into peer’s IP address */
  hp = gethostbyname(host);
  if (!hp) {
    fprintf(stderr, "simplex-talk: unknown host: %s\n", host);
    exit(1);
  }

  /* Build address data structure */
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  bcopy(hp->h_addr_list[0], (char *)&sin.sin_addr, hp->h_length);
  sin.sin_port = htons(SERVER_PORT);

  /* Active open */
  if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    perror("simplex-talk: socket");
    exit(1);
  }
  if (connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
    perror("simplex-talk: connect");
    close(s);
    exit(1);
  }

  /* Main loop: get and send lines of text */
  while (fgets(send_buf, sizeof(send_buf), stdin)) {
    // Send
    send_buf[MAX_LINE - 1] = '\0';
    recv_len = strlen(send_buf) + 1;
    send(s, send_buf, recv_len, 0);

    // Wait for echo
    send_len = recv(s, recv_buf, sizeof(recv_buf), 0);
    fputs(recv_buf, stdout);

    if (recv_len != send_len)
      fprintf(stderr, "simplex-talk: length differ (sent/recv): (%i/%i)\n",
              send_len, recv_len);
  }
}
