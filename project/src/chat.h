#ifndef CHAT_H
#define CHAT_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <map>
#include "messages.pb.h"
#include "data.h"
#include "values.h"

void auth(const int);
void handle_command(const std::string&);
void handle_response(const std::string&);


void who(const std::string&);
void exit(const int, const std::string&);
void send_msg(const chat::Command&);
void send_group(const chat::Command&);
void send_file(const chat::Command&);
void create_group(const chat::Command&);
void join_group(const chat::Command&);
void game(const chat::Command&);
void gamea(const chat::Command&);

#endif
